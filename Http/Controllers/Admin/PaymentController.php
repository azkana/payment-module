<?php

namespace Modules\Payment\Http\Controllers\Admin;

use Illuminate\Routing\Controller;

class PaymentController extends Controller
{
    protected $pageTitle;

    public function __construct()
    {
        $this->pageTitle = 'Payment Module';
    }
    
    public function index()
    {
        $params['pageTitle'] = $this->pageTitle;
        return view('payment::admin.index', $params);
    }
}
