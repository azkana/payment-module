<?php

namespace Modules\Payment\Entities;

use App\Models\BaseModel;

class PaymentModel extends BaseModel
{
    public const DB_TABLE_PREFIX = 'paym_';
}
