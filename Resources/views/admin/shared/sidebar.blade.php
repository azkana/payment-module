{{-- @can('payment') --}}
<li class="treeview @if($adminActiveMenu == 'payment') active @endif">
    <a href="#">
        <i class="fa fa-money"></i>
        <span>Payment</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            <span class="label label-danger pull-right">
                New
            </span>
        </span>
    </a>
    <ul class="treeview-menu">
        {{-- @can('payment-dashboard-read') --}}
        <li class="@if($adminActiveSubMenu == 'dashboard') active @endif">
            <a href="{!! route('payment.dashboard') !!}">
                <i class="fa fa-circle-o"></i> Dashboard
                <span class="pull-right-container"></span>
            </a>
        </li>
        {{-- @endcan --}}
        {{-- @can('ecommerce-product')
            <li class="@if($adminActiveSubMenu == 'product') active @endif">
                <a href="#"><i class="fa fa-circle-o"></i> Products
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('product-read')
                        <li class="@if($adminActiveSubMenu == 'product' && $urlSegment4 == 'product') active @endif">
                            <a href="{!! route('ecommerce.product.index') !!}">
                                <i class="fa fa-circle-o"></i> Product List
                                <span class="pull-right-container"></span>
                            </a>
                        </li>
                    @endcan
                    @can('product-category-read')
                        <li class="@if($adminActiveSubMenu == 'product' && $urlSegment4 == 'product-category') active @endif">
                            <a href="{!! route('ecommerce.product-category.index') !!}">
                                <i class="fa fa-circle-o"></i> Product Category
                                <span class="pull-right-container"></span>
                            </a>
                        </li>
                    @endcan
                    @can('product-attribute-read')
                        <li class="@if($adminActiveSubMenu == 'product' && $urlSegment4 == 'product-attribute') active @endif">
                            <a href="{!! route('ecommerce.product-attribute.index') !!}">
                                <i class="fa fa-circle-o"></i> Product Attribute
                                <span class="pull-right-container"></span>
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan --}}
        
    </ul>
</li>
{{-- @endcan --}}