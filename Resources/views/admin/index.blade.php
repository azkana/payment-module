@extends('payment::layouts.master')

@section('content')

    <p>
        This view is loaded from module: {!! config('payment.name') !!}
    </p>
@endsection
