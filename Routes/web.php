<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {
    Route::prefix('manage')->group(function() {
        Route::prefix('payment')->group(function() {
            Route::name('payment.')->group(function() {

                Route::get('dashboard', 'Admin\PaymentController@index')->name('dashboard');//->middleware('permission:ecommerce-dashboard-read');

                // Route::prefix('product')->group(function() {

                //     Route::prefix('product')->group(function() {
                //         Route::name('product.')->group(function() {
                //             Route::get('/', 'Admin\ProductController@index')->name('index');
                //         });
                //     });

                // });

            });
        });
    });
});
